import java.io.IOException;
import java.math.BigInteger;
import java.util.LinkedList;
import java.util.Scanner;

/**
 * @author Robin FASSINA-MOSCHINI
 * @version 06/01/2016.
 */
public class Main {

    public static void main(String[] args) {

        PlayerA alice = new PlayerA();
        /* test QR QNR
        BigInteger n = BigInteger.valueOf(11);
        BigInteger ace = BigInteger.valueOf(1);
        BigInteger six = BigInteger.valueOf(3);
        BigInteger deux = BigInteger.valueOf(2);
        System.out.println(alice.isQR(ace,n));
        System.out.println(alice.isQR(six,n));
        System.out.println(alice.isQR(deux,n));
        BigInteger q = alice.getPrimeNumberQ();
        BigInteger p = alice.getPrimeNumberP();
        alice.setN();
        alice.setON();
        alice.setE();
        alice.setD();
        System.out.println(alice.isQR(alice.encryptCard(ace),n));
        System.out.println(alice.isQR(alice.encryptCard(six),n));
        System.out.println(alice.isQR(alice.encryptCard(deux),n));*/
        System.out.println("Activate cheat [N]/Y:");
        Scanner in = new Scanner(System.in);
        String s = in.nextLine();
        Boolean cheat;
        switch (s) {
            case "Y":
                cheat = true;
                System.out.println("Cheat actived");
                break;
            case "N":
                cheat =false;
                break;
            default:
                cheat =false;
                break;
        }
        System.out.println("Activate AntiCheat [N]/Y:");
        in = new Scanner(System.in);
        s = in.nextLine();
        Integer bruit;
        switch (s) {
            case "Y":
                bruit = 5;
                System.out.println("antiCheat actived");
                break;
            case "N":
                bruit = 0;
                break;
            default:
                bruit = 0;
                break;
        }
        try {
            alice.openSocket();
        } catch (IOException e) {
            e.printStackTrace();
        }
        BigInteger q = (BigInteger) alice.receive();
        alice.setPrimeNumberQ(q);
        BigInteger p = alice.getPrimeNumberP();
        try {
            alice.send(p);
        } catch (IOException e) {
            e.printStackTrace();
        }
        alice.setN();
        alice.setON();
        alice.setE();
        alice.setD();
        LinkedList<BigInteger> deck = (LinkedList<BigInteger>) alice.receive();
        LinkedList<BigInteger> deckS = alice.shuffleDeck(deck);
        Integer indexA = 1;
        Integer indexB = 0;
        if (cheat){
            indexA = alice.cheatQuadraticResidue(deckS);
            indexB = (indexA+1) % deck.size();
        }
        LinkedList<BigInteger> deckC= new LinkedList<BigInteger>();
        deckC.add(deckS.get(indexB));
        BigInteger carteBob=deckS.get(indexB);
        deckC.add(alice.encryptCard(deckS.get(indexA)));
        try {
            alice.send(deckC);
        } catch (IOException e) {
            e.printStackTrace();
        }
        BigInteger aliceCardC=(BigInteger) alice.receive();
        BigInteger aliceCard=alice.decryptCard(aliceCardC, alice.d);
        System.out.println("Carte Alice: "+alice.getNameCard(aliceCard,bruit));
        try {
            alice.send(alice.d);
        } catch (IOException e) {
            e.printStackTrace();
        }
        BigInteger bobCle=(BigInteger) alice.receive();
        System.out.println("Carte Bob: "+alice.getNameCard(alice.decryptCard(carteBob,bobCle),bruit));
        try {
            alice.closeSocket();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    }
