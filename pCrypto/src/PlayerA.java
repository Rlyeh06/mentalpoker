import java.io.*;
import java.math.BigInteger;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.Random;

/**
 * @author Robin FASSINA-MOSCHINI
 * @version 06/01/2016.
 */
public class PlayerA {

    public BigInteger p;
    public BigInteger q;
    public BigInteger n;
    public BigInteger on;
    public Integer SIZE = 128;
    public BigInteger e;
    public BigInteger d;
    public ServerSocket MySocket;
    public Integer PortNumber = 1025;
    public Socket servSocket = null;
    public ObjectOutputStream oos;
    public ObjectInputStream in;
    //public String Bob = "134.59.191.33" ;

    public enum Card {
        ACE (1),
        DEUX (2),
        SIX (3);

        public Integer values;   // in kilograms
        Card(Integer values) {
            this.values = values;
        }
        private Integer valeur() { return values; }
    }



    /**
     * Constructor for PlayerA class
     */
    public PlayerA(){
        }

    public String getNameCard(BigInteger value, Integer n) {
        value=value.divide(BigInteger.valueOf(10).pow(n));
        for(Card e: Card.values()) {
            if(value.equals(BigInteger.valueOf(e.valeur()))) {
                return e.name();
            }
        }
        return ""+value.intValue();
    }

    public void openSocket() throws IOException {
        this.MySocket = new ServerSocket(this.PortNumber);
        System.out.println ("Waiting for connection.....");
        try {
            this.servSocket = this.MySocket.accept();
        }
        catch (IOException e)
        {
            System.err.println("Accept failed.");
            System.exit(1);
        }
        System.out.println ("Connection successful");
        this.in = new ObjectInputStream(this.servSocket.getInputStream());
        this.oos = new ObjectOutputStream(this.servSocket.getOutputStream());
    }

    public void closeSocket() throws IOException {
        this.in.close();
        this.oos.close();
        this.servSocket.close();
        this.MySocket.close();
    }


    public Object receive(){
        try {

            Object res;
            try {
                res= this.in.readObject();
                return res;
            } catch (ClassNotFoundException e1) {
                e1.printStackTrace();
                return -1;
            }

        } catch (IOException e1) {
            e1.printStackTrace();
            return -1;
        }

    }

    /**
     * The method used to shuffle the deck
     * @param deck
     */
    public LinkedList<BigInteger> shuffleDeck(LinkedList<BigInteger> deck){
        Integer newI;
        Integer i;
        Random randIndex = new Random();
        for (i = 0; i < deck.size(); i++) {
            // pick a rand num >= 0 && deck.length - 1
            newI = randIndex.nextInt(deck.size());
            // swap cards[i] and cards[newI]
            BigInteger temp = deck.get(i);
            deck.set(i, deck.get(newI));
            deck.set(newI, temp);
        }
        return deck;
    }

    /*public void send(){
        try {
            //output = new PrintStream(MyClient.getOutputStream());
        }
        catch (IOException e) {
            System.out.println(e);
        }

    }*/

    public BigInteger getPrimeNumberP(){
        this.p=BigInteger.probablePrime(SIZE, new Random());
        return this.p;
    }

    public BigInteger getPrimeNumberQ(){
        this.q=BigInteger.probablePrime(SIZE, new Random());
        return this.q;
    }

    public void setPrimeNumberQ(BigInteger q){
        this.q=q;
    }

    public void setN(){
        this.n=this.p.multiply(this.q);
    }

    public void setON(){
        this.on=(this.p.subtract(BigInteger.valueOf(1))).multiply(this.q.subtract(BigInteger.valueOf(1)));
    }

    public void setE(){
        do{
            this.e = new BigInteger(2*SIZE, new Random());
        }while( (this.e.equals(this.on))|| (this.e.gcd(this.on).intValue() != 1));
    }

    public void setD(){
        this.d = this.e.modInverse(this.on);
    }

    public BigInteger decryptCard(BigInteger card, BigInteger cle){
        return card.modPow(cle,this.n);
    }

    public BigInteger encryptCard(BigInteger card){
        return card.modPow(this.e,this.n);
    }

    public void send(Object o) throws IOException {
        this.oos.writeObject(o);
    }

    public Integer cheatQuadraticResidue(LinkedList<BigInteger> deck){
        Integer i;
        for (i = 0; i < deck.size(); i++){
            if(isQR(deck.get(i), BigInteger.valueOf(8))){
                System.out.println("QR found");
                return i;
            }
        }
        System.out.println("QR not found");
        return 1;
    }

    public boolean isQR(BigInteger card, BigInteger n){
        if (card.gcd(n).equals(BigInteger.valueOf(1))){
            BigInteger x=BigInteger.valueOf(1);
            for (int i=1; i<n.intValue();i++){
                if((x.modPow(BigInteger.valueOf(2),n).compareTo(card.mod(n)))==0){
                    return true;
                }
            x.add(BigInteger.valueOf(1));
            }
        }
        return false;
    }

}
